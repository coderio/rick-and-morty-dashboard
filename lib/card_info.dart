import 'package:flutter/material.dart';
import 'constants.dart';

class CardInfo extends StatelessWidget {
  final String title;
  final int amount;
  final String cardImage;

  const CardInfo({
    Key? key,
    required this.title,
    required this.amount,
    required this.cardImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        image: DecorationImage(
          fit: BoxFit.fill,
          image: NetworkImage(cardImage),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Container(
                  child: Column(
                    children: [
                      Text(amount.toString(),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .headline3!
                              .copyWith(color: txtColor)),
                      Text(title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .headline4!
                              .copyWith(color: txtColor)),
                    ],
                  ),
                  //padding: EdgeInsets.all(defaultPadding * 0.75),
                  //height: 80,
                  //width: 80,
                  decoration: BoxDecoration(
                    color: cardBackGroundColor,
                    // image: DecorationImage(
                    //   fit: BoxFit.fill,
                    //   image: NetworkImage(cardImage),
                    //),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
