import 'package:flutter/material.dart';
import 'constants.dart';
import 'cardinfo_container_desktop.dart';
import 'cardinfo_container_mobile.dart';
import 'responsive.dart';

/// Creamos el primer statefulwidget
/// Estos widgets pueden cambiar durante el ciclo de vida de la app.
class DashboardScreen extends StatefulWidget {
  /// Este es el constructor que es unas constante y recibe un parametro nombrado.
  /// llamado key que sirve para
  const DashboardScreen({Key? key}) : super(key: key);

  @override

  ///Inicializamos el estado del widget.
  _DashboardScreenState createState() => _DashboardScreenState();
}

/// Creamos el estado que una clase privada _
class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    /// safe area evita que el contenido se muestre sobre interfases del sistema.
    return SafeArea(
      /// Permite scrolear
      child: SingleChildScrollView(
        /// Agregamos algo de padding
        padding: EdgeInsets.all(defaultPadding),

        /// Finalmente creamos el hijo del SingleChildScrollView que es
        /// una columna ocupando todo el ancho de pantalla.
        /// las columnas tienes hijos, lo que permite incluir varios widgets.
        child: Column(
          children: [
            if (Responsive.isDesktop(context)) CardInfoContainer(),
            if (Responsive.isMobile(context)) CardInfoContainerMobile(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 5,
                  child: Column(
                    children: [
                      Placeholder(),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Placeholder(),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
