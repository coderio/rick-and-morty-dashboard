import 'package:flutter/material.dart';
import 'package:rick_and_morty_dashboard/dashboard_screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'constants.dart';

/// Creamos el Widget MyApp
/// StatelessWidget Es un Widget que se construye solo con configuraciones
/// que se iniciaron desde el principio.
/// Entonces Stateless Widget es un widget que nunca cambiará.
/// eso no quiere decir que los widgets hijos no puedan ser diferentes
/// dentro del arbol de widgets.
///
class MyApp extends StatelessWidget {
  /// La anotación @override es para indicar que el  siguiente metodo se sobreescribira.
  @override

  /// Damos inicion al arbol de widgets con el metodo build pasandole el contexto,
  /// que es el encargado de llevar el control de la ubicacion de cada widget dentro del arbol.
  Widget build(BuildContext context) {
    /// MaterialApp es un widget que nos proporciona un layout orientado a Material Design
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Rick And Morty Dashboard', // Titulo de la App.
      /// Define la configuración del visual general
      /// tenemos varias opciones que nos brinda este widget.
      theme: ThemeData(
        primaryColor: primaryColor,
        scaffoldBackgroundColor: bgColor,
        canvasColor: bgColor,
        textTheme:
            GoogleFonts.patrickHandScTextTheme(Theme.of(context).textTheme)
                .apply(bodyColor: txtColor),
      ),

      /// Agreagamos  Scafold.
      home: Scaffold(
        drawer: Drawer(
          child: ListView(
            children: [
              DrawerHeader(
                child: CircleAvatar(
                  radius: 25,
                  backgroundColor: Colors.blue,
                  backgroundImage:
                      AssetImage("assets/images/drawer_header.png"),
                ),
              ),
              ListTile(
                title: Text('Dashboard'),
                leading: Icon(
                  Icons.admin_panel_settings_sharp,
                  color: primaryColor,
                ),
              )
            ],
          ),
        ),
        appBar: AppBar(
          elevation: 20.0,
          shadowColor: shadowColor,
          backgroundColor: bgColor,
          iconTheme: IconThemeData(color: iconColor),
          title: Center(child: Image.asset("assets/images/logo.png")),
        ),
        body: DashboardScreen(),
      ),
    );
  }
}
