import 'package:flutter/material.dart';


const primaryColor = Color(0xFFA1BF34);
const secondaryColor = Color(0xFFF2D335);
const bgColor = Color(0xFF233559);
const txtColor = Color(0xFFF2F2F2);

const defaultPadding = 32.0;

