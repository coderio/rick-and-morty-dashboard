import 'package:flutter/material.dart';
import 'card_info.dart';

class CardInfoContainerMobile extends StatelessWidget {
  const CardInfoContainerMobile({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CardInfo(
              title: 'Personajes',
              amount: 671,
              cardImage:
                  "https://rickandmortyapi.com/api/character/avatar/671.jpeg",
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CardInfo(
              title: 'Lugares',
              amount: 108,
              cardImage:
                  "https://static0.srcdn.com/wordpress/wp-content/uploads/2020/05/Rick-and-Morty-Rick-and-Planets-Only.jpg?q=50&fit=crop&w=960&h=500&dpr=1.5",
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CardInfo(
              title: 'Capitulos',
              amount: 41,
              cardImage:
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQb_c1pGF8fGcgCpObuFwX80YqaXtGr9d2bDg&usqp=CAU",
            ),
          ),
        ],
      ),
    );
  }
}
