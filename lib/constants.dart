import 'package:flutter/material.dart';

/// Usamos el Widget Color para generar colores RGB y Hexa
const primaryColor = Color(0xFFA1BF34);
const secondaryColor = Color(0xFFF2F2F2);
const bgColor = Color(0xFF000000);
const txtColor = Color(0xFFF2F2F2);
const shadowColor = Color(0xFF41BEDC);
const iconColor = Color(0xFF41BEDC);

/// Creamos colores RGB
const cardBackGroundColor = Color.fromRGBO(22, 28, 36, 0.72);
const defaultPadding = 32.0;
